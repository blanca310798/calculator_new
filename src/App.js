import useForm from "./utility/useForm";
import HeaderComponent from './components/HeaderComponent';
import './css/style.css';

function App() {

  const { inputs, handleChange, clearForm, resetForm } = useForm({
    altura: "Escribe tu altura",
    peso: "Escribe tu peso",
    cintura: "Medida de tu cintura",
    cuello: "Medida de tu cuello",
    genero: "F"
  });
  
  return (
      <div className="App" style={{ padding: "20px" }}>
        
        <header>
          <HeaderComponent></HeaderComponent>
        </header>

        <h2>Calculadora de masa corporal</h2>
        <div className="txhe"> El método de la Marina de Estados Unidos (US Navy Method) ofrece una manera sencilla de cancelar un aproximado del porcentaje de tejido adiposo en el cuerpo de una persona. </div>
        <div className="txhe"> Los valores requeridos por la fórmula son los siguientes: </div>
        <form>
        <span>Género</span>
          <br />
          <label>
            <input type="radio"
              name="genero"
              value="F"
              checked={inputs.genero === "F"}
              onChange={handleChange} />Mujer</label>
          <label>
            <input type="radio"
              name="genero"
              value="M"
              checked={inputs.genero === "M"}
              onChange={handleChange} />Hombre</label>
          <br />

          <label htmlFor="altura">Altura (cm) </label>
          <br />
          <input type="text"
            name="altura"
            value={inputs.altura}
            onChange={handleChange} />
          <br />

          <label htmlFor="peso">Peso (kg) </label>
          <br />
          <input type="text"
            name="peso"
            value={inputs.peso}
            onChange={handleChange} />
          <br />

          <label htmlFor="cintura">Cintura (cm) </label>
          <br />
          <input type="text"
            name="cintura"
            value={inputs.cintura}
            onChange={handleChange} />
          <br />

          <label htmlFor="cuello">Cuello (cm) </label>
          <br />
          <input type="text"
            name="cuello"
            value={inputs.cuello}
            onChange={handleChange} />
          <br />
          
          <button className="redondo1" type="button" onClick={resetForm}>Calcular</button>
          <button className="redondo2" type="button" onClick={clearForm}>Limpiar</button>
          
        </form>
      </div>
  );
}

export default App;